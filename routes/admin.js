import express from 'express';
import { addDataByAdmin, adminLogin } from '../controllers/adminController.js';
// import path from 'path';
// import { fileURLToPath } from 'url';

const routerAdmin = express.Router();

// const __dirname = path.dirname(fileURLToPath(import.meta.url));
// const publicPath = path.join(__dirname, "../");

//http://localhost:3000/admin/
routerAdmin.get("/", (req, res) => {
    res.send("Welcome to admin Panel! Enter Username and Password in URL to login");
});

routerAdmin.get("/:username&:password", adminLogin);

routerAdmin.get("/add/:character&:anime", addDataByAdmin);

export default routerAdmin;