import express from 'express';
import { findCharacter, getCharacterData } from '../controllers/characterController.js';

// middleware that is specific to this router 
const routerCharacter = express.Router(); // Modular Routing

//http://localhost:3000/characters/
routerCharacter.get("/", getCharacterData);

routerCharacter.get("/:characterName", findCharacter);

export default routerCharacter;