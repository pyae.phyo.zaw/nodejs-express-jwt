import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from "body-parser"; // take incoming post request bodies
import characterRoutes from "./routes/characters.js";
import adminRoutes from "./routes/admin.js";

const app = express();

const port = 3000;

app.use(bodyParser.json());
app.use(cookieParser());
app.use("/characters", characterRoutes);
app.use("/admin", adminRoutes);

app.get("/", (req, res) => {
    res.send("Welcome to Main Page!");
});

app.listen(port, () => {
    console.log(`Server is running on Port ${port}`);
});