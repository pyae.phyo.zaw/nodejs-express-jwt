import { addData } from "./characterController.js";
import fs from 'fs';
import jwt from 'jsonwebtoken';

const life = 30000; // millisecond
const secrect_key = "go go power rangers";

const loadAdminData = () => {
    try {
        let dataBuffer = fs.readFileSync("./admin.json");
        let dataJSON = dataBuffer.toString();
        let data = JSON.parse(dataJSON);
        return data;
    } catch (e) {
        return [];
    }
};

export const adminLogin = async (req, res, next) => {
    let { username, password } = req.params;
    checkAdmin(username, password, res, next);
};

export const addDataByAdmin = async (req, res, next) => {
    let token = req.cookies.token;
    let data = await loadAdminData();
    let { character, anime } = req.params;
    console.log(character, anime);
    // res.send(req.params);
    try {
        let adm = jwt.verify(token, secrect_key);
        const admin = data.find((d) => d.adminName === adm.un);
        if (admin) {
            console.log("You can add data now");
            addData(character, anime);
            res.send(`Character name ${character}and anime name ${anime} is added now`);
        }
        next();
    } catch (e) {
        res.clearCookie("token");
        return res.redirect("/admin");
    }
};

const checkAdmin = async (un, psw, res) => {
    let data = await loadAdminData();
    if (data) {
        const admin = data.find((d) => d.adminName === un && d.password === psw);
        if (admin) {
            const token = createToken(un);
            res.cookie("token", token, {});
            res.send("Login Success!");
        } else {
            res.send("Invalid Login");
        }
    }
};

const createToken = (un) => {
    return jwt.sign({ un }, secrect_key, {
        expiresIn: life
    });
};



