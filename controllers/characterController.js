import fs from 'fs';

export const addData = (cn, an) => {
    const data = loadData();

    const duplicateCharacter = data.find((d) => d.characterName === cn);
    if (!duplicateCharacter) {
        data.push({
            characterName: cn,
            animeName: an,
        });
        console.log("New Character is added");
        saveData(data);
    } else {
        console.log("Character is already added!");
    }
};

const saveData = (data) => {
    const dataJSON = JSON.stringify(data);
    fs.writeFileSync("./characters.json", dataJSON);
};

const loadData = () => {
    try {
        let dataBuffer = fs.readFileSync("./characters.json");
        let dataJSON = dataBuffer.toString();
        let data = JSON.parse(dataJSON);
        return data;
    } catch (e) {
        return [];
    }
};

export const getCharacterData = async (req, res) => {
    let data = await loadData();
    if (data.length === 0) res.send("No Character Data. Please contact admin to add character data");
    else res.send(data);
};

export const findCharacter = async (req, res) => {
    let { characterName } = req.params;
    let data = await loadData();
    const foundChar = data.find((d) => d.characterName.toLowerCase() === characterName.toLowerCase());
    if (foundChar) res.send(foundChar);
    else res.send(`Can't find Character name : ${characterName}`);
};


